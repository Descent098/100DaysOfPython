import datetime

def get_birthday_from_user():
    year = eval(input("\nWhat year were you born [YYYY]: "))
    month = eval(input("\nWhat month were you born [MM]: "))
    day = eval(input("\nWhat day were you born [DD]: "))
    return datetime.date(year, month, day)

def compute_days_until_birthday(original_date, target_date):
    this_year = datetime.date(target_date.year, original_date.month, original_date.day)
    delta = this_year - target_date
    return delta.days

def nice_print_days(days, age):
    if days <0:
        print("Your birthday happened {} days ago this year!".format(abs(days)))
        print("congratulations you turned {} years old!".format(age))
    elif days>0:
        print("Your birthday is in {} days!".format(days))
        print("You will be {} years old!".format(age))
    else:
        print("Happy Birthmas!")
        print("Cheers to another {} years!".format(age))

def determine_age(original_date, target_date):
    delta = abs(target_date.year - original_date.year)
    return delta



def main():
    print("Welcome to the app, this will determine how long until your birthday")
    todaydate = datetime.date.today()
    bday = get_birthday_from_user()
    day_delta = compute_days_until_birthday(bday,todaydate)
    nice_print_days(day_delta, determine_age(bday, todaydate))


main()
