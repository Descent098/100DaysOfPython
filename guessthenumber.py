import random
number, correctnumber = random.randint(0,30), False
while (correctnumber==False):
    guess = eval(input("Guess the number"))
    if guess > number:
        print("Too large")
    elif guess < number:
        print("Too Small")
    elif guess == number:
        print("Just right")
        correctnumber = True
